  name := "bibit"
  version := "0.0.1"
 // scalaVersion := "2.11.8"
  scalaVersion := "2.12.1"

  scalacOptions += "-unchecked"
  scalacOptions += "-deprecation"
  scalacOptions += "-feature"

  libraryDependencies ++= Seq(
    "org.scalatest" %% "scalatest" % "3.0.1" % "test"
  )
