package object bibit {
  def calc(bits: String, debug: Boolean = false): String = {
    def aux(in: Pyramid): Char = in match {
      case S(b) => b
      case _ =>
        if (debug)
          println(Pyramid.toString(in))
        aux(Pyramid.iteration(in))
    }
    (aux(Pyramid(bits)) :: Nil).mkString
  }
}