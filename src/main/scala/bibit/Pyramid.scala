package bibit

import math._

sealed abstract class Pyramid
case class S(value: Char) extends Pyramid
case class Q(b0: Pyramid, b1: Pyramid, b2: Pyramid, b3: Pyramid) extends Pyramid

object Pyramid {
  def apply(bits: String): Pyramid =
    toPyramid(toLines(normalize(bits)))

  def toString(in: Pyramid): String = {
    val lines = toLines(in)
    lines.map(_.reverse).reverse.mkString
  }

  def iteration(in: Pyramid): Pyramid = {
    val r = step(in)
    if (r != in) r
    else simplify(r)
  }

  def step(in: Pyramid): Pyramid = in match {
    case S(value) => in
    case Q( S(b0), S(b1), S(b2), S(b3) ) => matchStep(in)
    case Q( b0, b1, b2, b3 ) => Q(step(b0), step(b1), step(b2), step(b3))
  }

  def simplify(in: Pyramid): Pyramid = {
    val simplied = in match {
      case S(value) => in
      case Q( S('0'), S('0'), S('0'), S('0') ) => S('0')
      case Q( S('1'), S('1'), S('1'), S('1') ) => S('1')
      case Q( b0, b1, b2, b3 ) => Q(simplify(b0), simplify(b1), simplify(b2), simplify(b3))
    }
    simplied match {
      case Q( S('0'), S('0'), S('0'), S('0') ) => S('0')
      case Q( S('1'), S('1'), S('1'), S('1') ) => S('1')
      case _ => simplied
    }
  }

  private def matchStep(in: Pyramid): Pyramid = in match {
    case Q( S('0'), S('0'), S('0'), S('0') ) => Q( S('0'), S('0'), S('0'), S('0') )
    case Q( S('1'), S('0'), S('0'), S('0') ) => Q( S('0'), S('0'), S('0'), S('1') )
    case Q( S('0'), S('1'), S('0'), S('0') ) => Q( S('1'), S('0'), S('0'), S('0') )
    case Q( S('1'), S('1'), S('0'), S('0') ) => Q( S('0'), S('1'), S('0'), S('0') )
    case Q( S('0'), S('0'), S('1'), S('0') ) => Q( S('0'), S('0'), S('0'), S('0') )
    case Q( S('1'), S('0'), S('1'), S('0') ) => Q( S('0'), S('1'), S('0'), S('0') )
    case Q( S('0'), S('1'), S('1'), S('0') ) => Q( S('1'), S('1'), S('0'), S('1') )
    case Q( S('1'), S('1'), S('1'), S('0') ) => Q( S('1'), S('1'), S('0'), S('1') )
    case Q( S('0'), S('0'), S('0'), S('1') ) => Q( S('0'), S('0'), S('1'), S('0') )
    case Q( S('1'), S('0'), S('0'), S('1') ) => Q( S('1'), S('0'), S('1'), S('0') )
    case Q( S('0'), S('1'), S('0'), S('1') ) => Q( S('1'), S('1'), S('1'), S('0') )
    case Q( S('1'), S('1'), S('0'), S('1') ) => Q( S('1'), S('1'), S('1'), S('1') )
    case Q( S('0'), S('0'), S('1'), S('1') ) => Q( S('1'), S('0'), S('1'), S('1') )
    case Q( S('1'), S('0'), S('1'), S('1') ) => Q( S('0'), S('1'), S('1'), S('1') )
    case Q( S('0'), S('1'), S('1'), S('1') ) => Q( S('1'), S('1'), S('1'), S('0') )
    case Q( S('1'), S('1'), S('1'), S('1') ) => Q( S('1'), S('1'), S('1'), S('1') )
    case _ => throw new Error("Impossible Pyramid!")
  }

  private[bibit] def toLines(bits: String) = {
    val h = sqrt(bits.length).toInt
    val len = 2*h - 1

    def aux(xs: String, len: Int, acc: List[String]): Array[String] =
      if (len == 1)
        (xs :: acc).toArray
      else {
        val (x1, x2) = xs.splitAt(len)
        aux(x2, len - 2, x1.reverse :: acc)
      }
    aux(bits, len, Nil)
  }

  private[bibit] def toPyramid(ls: Array[String]): Pyramid = {
    def aux(h: Int, forward: Boolean, top: (Int, Int)): Pyramid = {
      if (h == 1) {
        S( ls(top._1)(top._2))
      }
      else {
        val hh = h / 2
        val mul = if (forward) 1 else -1
        Q (aux(hh, forward, top),
           aux(hh, forward, (top._1 + mul * hh, (if (forward) top._2 else top._2 - h))),
           aux(hh, !forward, (top._1 + mul*(h - 1), top._2 + mul*(h -1))),
           aux(hh, forward, (top._1 + mul * hh, (if (forward) top._2 + h else top._2)))
          )
      }
    }
    aux(ls.length, true, (0, 0))
  }

  private[bibit] def toLines(in: Pyramid): Array[String] = {
    val height = getHeight(in)
    val arr = (1 to height).map(h => ("0" * (h*2 - 1)).toArray).toArray

    def aux(h: Int, forward: Boolean, top: (Int, Int), in: Pyramid): Unit = {
      val mul = if (forward) 1 else -1
      in match {
        case S(v) =>
          for {
            ch <- 0 until h
            l = top._1 + mul * ch
            c <- top._2 until (top._2 + mul * (2*ch + 1)) by mul
          } {
            arr(l)(c) = v
          }
        case Q(b0, b1, b2, b3) =>
          val hh = h / 2
          aux(hh, forward, top, b0)
          aux(hh, forward, (top._1 + mul * hh, (if (forward) top._2 else top._2 - h)), b1)
          aux(hh, !forward, (top._1 + mul*(h - 1), top._2 + mul*(h -1)), b2)
          aux(hh, forward, (top._1 + mul * hh, (if (forward) top._2 + h else top._2)), b3)
      }
    }

    aux(height, true, (0,0), in)
    arr.map(_.mkString)
  }

  private[bibit] def getHeight(in: Pyramid): Int = {
    def aux(in: Pyramid, height: Int): Int = in match {
      case S(_) => height
      case Q(b0, b1, b2, b3) =>
        val h = height * 2
        aux(b0, h) max aux(b1, h) max aux(b2, h) max aux(b3, h)
    }
    aux(in, 1)
  }

  private[bibit] def normalize(bits: String): String = {
    val len = bits.length
    val expected = if (len <= 1) 4
                   else pow(4, ceil(log(len) / log(4))).toInt
    "0" * (expected - len) + bits
  }
}
