package bibit

import org.scalatest.FunSuite

class BiBitSuite extends FunSuite {
  test("0100 -> 0") {
    assert(calc("0100") === "0")
  }

  test("1110111100111000 -> 1") {
    assert(calc("1110111100111000") === "1")
  }

  test("1100100100001111110110101010001000100001011010001100001000110100 -> 1") {
    assert(calc("1100100100001111110110101010001000100001011010001100001000110100") === "1")
  }
}