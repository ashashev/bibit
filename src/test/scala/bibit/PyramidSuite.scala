package bibit

import org.scalatest.FunSuite

class PyramidSuite extends FunSuite {
  test("one bit to lines") {
    assert(Pyramid.toLines("1") === Array("1"))
    assert(Pyramid.toLines("0") === Array("0"))
  }

  test("four bits to lines") {
    assert(Pyramid.toLines("0000") === Array("0", "000"))
    assert(Pyramid.toLines("1101") === Array("1", "011"))
    assert(Pyramid.toLines("0101") === Array("1", "010"))
  }

  test("sixteen bits to lines") {
    assert(Pyramid.toLines("0101110110100110") === Array("0", "110", "01011", "0111010"))
  }

  test("one line to Pyramid") {
    assert(Pyramid.toPyramid(Array("0")) === S('0'))
  }

  test("two line to Pyramid") {
    assert(Pyramid.toPyramid(Array("1", "011")) === Q(S('1'), S('0'), S('1'), S('1')))
  }

  test("four line to Pyramid") {
/*
          ^
         /0\ 
        *---*
       /1\1/0\
      *---*---*
     /0\1/0\1/1\
    *---*---*---*
   /0\1/1\1/0\1/0\
  *---*---*---*---*
*/
    assert(Pyramid.toPyramid(Array("0", "110", "01011", "0111010")) === 
      Q( Q(S('0'), S('1'), S('1'), S('0')),
         Q(S('0'), S('0'), S('1'), S('1')),
         Q(S('1'), S('1'), S('0'), S('1')),
         Q(S('1'), S('0'), S('1'), S('0'))
        ) )
  }

  test("getHeight for single elem") {
    val p = S('0')
    assert(Pyramid.getHeight(p) === 1)
  }

  test("getHeight for single quads") {
    val p = Q( S('0'), S('1'), S('1'), S('1') )
    assert(Pyramid.getHeight(p) === 2)
  }

  test("getHeight for quad with quad") {
    val p = Q( S('1'), S('1'), Q(S('1'),S('0'),S('0'),S('1')), S('1') )
    assert(Pyramid.getHeight(p) === 4)
  }

  test("single to lines") {
    assert(Pyramid.toLines(S('1')) === Array("1"))
  }

  test("quad to lines") {
    val p = Q(S('0'),S('1'),S('1'),S('0'))
    assert(Pyramid.toLines(p) === Array("0", "110"))
  }

  test("quad to lines 2") {
    val p = Q(S('1'),S('0'),S('1'),S('1'))
    assert(Pyramid.toLines(p) === Array("1", "011"))
  }

  test("quad with quad to lines") {
    val p = Q( S('1'), S('0'), Q(S('1'),S('0'),S('0'),S('1')), S('1') )
    assert(Pyramid.toLines(p) === Array("1", "111", "00011", "0001111"))
  }

  test("string -> pyramid -> string") {
    val bits = "1100100100001111110110101010001000100001011010001100001000110100"
    val p = Pyramid(bits)
    val res = Pyramid.toString(p)
    assert(bits === res)
  }

  test("normalize correct bits") {
    assert(Pyramid.normalize("0101110110100110") === "0101110110100110")
  }

  test("normalize single bit 0") {
    assert(Pyramid.normalize("0") === "0000")
  }

  test("normalize single bit 1") {
    assert(Pyramid.normalize("1") === "0001")
  }

  test("normalize five bits") {
    assert(Pyramid.normalize("01010") === "0000000000001010")
  }

}